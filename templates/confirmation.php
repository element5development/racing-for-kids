<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head page-head is-narrow">
	<?php if ( get_field('page_title') ) : ?>
	<h1><?php the_field('page_title'); ?></h1>
	<?php else : ?>
	<h1><?php the_title(); ?></h1>
	<?php endif; ?>

	<p><?php the_field('message'); ?></p>
</header>

<?php get_template_part('template-parts/sections/donation-callout'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>