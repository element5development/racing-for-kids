<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<section class="is-narrow aligncenter">
			<h1>404 Page Not Found</h1>
			<p>We can't find this page. It might be an old link or it may have moved.</p>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/donation-callout'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>