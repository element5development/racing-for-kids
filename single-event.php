<?php 
/*----------------------------------------------------------------*\

	EVENT SINGLE POST TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/event-header'); ?>

<main id="main-content">
	<article>
		<section class="editor is-narrow">
			<?php the_field('event_description'); ?>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/sponsors'); ?>

<?php get_template_part('template-parts/sections/donation-callout'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>