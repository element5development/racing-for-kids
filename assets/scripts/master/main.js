var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		SELECT FIELD PLACEHOLDER
	\*----------------------------------------------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		NOTIFICATION BAR
	\*----------------------------------------------------------------*/
	if (readCookie('cookieNotification') === 'false') {
		$('.cookie-useage-notification').removeClass("note-on");
	} else {
		$('.cookie-useage-notification').addClass("note-on");
	}
	$('.cookie-useage-notification button').click(function () {
		$('.cookie-useage-notification').removeClass("note-on");
		createCookie('cookieNotification', 'false');
	});

	if (readCookie('cookiePopup') === 'false') {
		$('.featured-popup').removeClass("popup-on");
		$('body.home').removeClass("popup-on");
	} else {
		$('.featured-popup').addClass("popup-on");
		$('body.home').addClass("popup-on");
	}
	$('.featured-popup button').click(function () {
		$('.featured-popup').removeClass("popup-on");
		$('body.home').removeClass("popup-on");
		createCookie('cookiePopup', 'false');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	/*----------------------------------------------------------------*\
  	ACTIVATE MENU
	\*----------------------------------------------------------------*/
	$("button.activate-menu").click(function () {
		$(".primary-navigation nav > div").addClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	CLOSE OVERLAY
	\*----------------------------------------------------------------*/
	$("button.close").click(function () {
		$(".primary-navigation nav > div").removeClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	TESTIMONIAL SLIDER
	\*----------------------------------------------------------------*/
	$('.testimonial-slider').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M3.52 3.52C1.172 5.864 0 8.691 0 12c0 3.308 1.173 6.135 3.52 8.48C5.864 22.828 8.691 24 12 24c3.308 0 6.135-1.173 8.48-3.52C22.828 18.136 24 15.309 24 12c0-3.308-1.173-6.135-3.52-8.48C18.136 1.172 15.309 0 12 0 8.692 0 5.865 1.173 3.52 3.52zM15.691 12l-5.538 5.538V6.462L15.692 12z" fill="#C11414" fill-rule="evenodd"/></svg>',
		nextArrow: '<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M3.52 3.52C1.172 5.864 0 8.691 0 12c0 3.308 1.173 6.135 3.52 8.48C5.864 22.828 8.691 24 12 24c3.308 0 6.135-1.173 8.48-3.52C22.828 18.136 24 15.309 24 12c0-3.308-1.173-6.135-3.52-8.48C18.136 1.172 15.309 0 12 0 8.692 0 5.865 1.173 3.52 3.52zM15.691 12l-5.538 5.538V6.462L15.692 12z" fill="#C11414" fill-rule="evenodd"/></svg>',
	});
});