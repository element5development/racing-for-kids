<?php 
/*----------------------------------------------------------------*\

	PHOTOGALLERY SINGLE POST TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/page-header'); ?>

<main id="main-content">
	<article class="gallery is-extra-wide columns-3">
		<?php $gallery = get_field('gallery'); ?>
		<?php foreach( $gallery as $gallery ): ?>
			<figure>
				<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $gallery['sizes']['placeholder']; ?>" data-src="<?php echo $gallery['sizes']['medium']; ?>" data-srcset="<?php echo $gallery['sizes']['small']; ?> 350w, <?php echo $gallery['sizes']['medium']; ?> 700w, <?php echo $gallery['sizes']['medium']; ?> 1000w, <?php echo $gallery['sizes']['medium']; ?> 1200w"  alt="<?php echo $gallery['alt']; ?>">
			</figure>
			<?php endforeach; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/related-photogallery'); ?>

<?php get_template_part('template-parts/sections/sponsors'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>