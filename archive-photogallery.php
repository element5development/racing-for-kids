<?php 
/*----------------------------------------------------------------*\

	PHOTOGALLERY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head is-narrow">
	<h1><?php the_field($post_type.'_title','options'); ?></h1>

	<?php if ( get_field($post_type.'_intro','options') ) : ?>
	<p class="subtitle"><?php the_field($post_type.'_intro','options'); ?></p>
	<?php endif; ?>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="photogallery-grid is-extra-wide">

				<?php	while ( have_posts() ) : the_post(); ?>

					<article class="archive-result <?php echo $post_type; ?>">
						<?php $gallery = get_field('gallery', $photo_gallery->ID); ?>

						<a href="<?php the_permalink(); ?>">
							<div class="photo-stack">
								<?php $image1 = $gallery[0]; ?>
								<?php $image2 = $gallery[1]; ?>
								<?php $image3 = $gallery[2]; ?>

								<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image1['sizes']['placeholder']; ?>" data-src="<?php echo $image1['sizes']['small']; ?>" data-srcset="<?php echo $image1['sizes']['small']; ?> 350w, <?php echo $image1['sizes']['small']; ?> 700w, <?php echo $image1['sizes']['small']; ?> 1000w, <?php echo $image1['sizes']['small']; ?> 1200w"  alt="<?php echo $image1['alt']; ?>">
								<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image2['sizes']['placeholder']; ?>" data-src="<?php echo $image2['sizes']['small']; ?>" data-srcset="<?php echo $image2['sizes']['small']; ?> 350w, <?php echo $image2['sizes']['small']; ?> 700w, <?php echo $image2['sizes']['small']; ?> 1000w, <?php echo $image2['sizes']['small']; ?> 1200w"  alt="<?php echo $image2['alt']; ?>">
								<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image3['sizes']['placeholder']; ?>" data-src="<?php echo $image3['sizes']['small']; ?>" data-srcset="<?php echo $image3['sizes']['small']; ?> 350w, <?php echo $image3['sizes']['small']; ?> 700w, <?php echo $image3['sizes']['small']; ?> 1000w, <?php echo $image3['sizes']['small']; ?> 1200w"  alt="<?php echo $image3['alt']; ?>">
								<div class="button">View</div>
							</div>
							<header>
								<h3><?php the_title(); ?></h3>
							</header>
						</a>
					</article>

				<?php endwhile; ?>

			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<h2>Coming soon.</h2>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/sponsors'); ?>

<?php get_template_part('template-parts/sections/donation-callout'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>