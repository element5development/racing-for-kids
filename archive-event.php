<?php 
/*----------------------------------------------------------------*\

	EVENT ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head is-narrow">
	<h1><?php the_field($post_type.'_title','options'); ?></h1>

	<?php if ( get_field($post_type.'_intro','options') ) : ?>
	<p class="subtitle"><?php the_field($post_type.'_intro','options'); ?></p>
	<?php endif; ?>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>

			<section class="is-extra-wide">
				<?php	while ( have_posts() ) : the_post(); ?>

					<?php $upcoming = get_field('upcoming_event'); ?>
					<?php if ( $upcoming ) : ?>
						<article class="archive-result <?php echo $post_type; ?>">
							<div>
								<h2><?php the_title(); ?></h2>
								<p><?php the_field('event_short_description'); ?></p>
								<div class="event-info">
									<h3><?php the_field('event_date'); ?></h3>
									<p><?php the_field('event_location'); ?></p>
								</div>
								<a class="button" href="<?php the_permalink(); ?>">Learn More</a>
							</div>
							<div class="image-wrap">
								<?php $event_image = get_field('event_photo'); ?>
								<?php if( $event_image ): ?>
									<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $event_image['sizes']['placeholder']; ?>" data-src="<?php echo $event_image['sizes']['medium']; ?>" data-srcset="<?php echo $event_image['sizes']['small']; ?> 350w, <?php echo $event_image['sizes']['medium']; ?> 700w, <?php echo $event_image['sizes']['medium']; ?> 1000w, <?php echo $event_image['sizes']['medium']; ?> 1200w"  alt="<?php echo $event_image['alt']; ?>">
								<?php else : ?>
									<?php $defaultimage = get_field('default_news_image', 'options'); ?>
									<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $defaultimage['sizes']['placeholder']; ?>" data-src="<?php echo $defaultimage['sizes']['medium']; ?>" data-srcset="<?php echo $defaultimage['sizes']['small']; ?> 350w, <?php echo $defaultimage['sizes']['medium']; ?> 700w, <?php echo $defaultimage['sizes']['medium']; ?> 1000w, <?php echo $defaultimage['sizes']['medium']; ?> 1200w"  alt="<?php echo $defaultimage['alt']; ?>">
								<?php endif; ?>
							</div>
						</article>

					<?php endif; ?>

				<?php endwhile; ?>
			</section>
			
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<h2>Coming soon.</h2>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php $args = array(
		'post_type' => 'event',
		'posts_per_page' => 3,
		'meta_query' => array(
			array(
					'key'   => 'upcoming_event',
					'value' => '0',
			)
		),
	);
?>
<?php $loop = new WP_Query( $args ); ?>

<?php if( $loop->have_posts() ): ?>
	<section class="recent-events">
		<div class="is-narrow">
			<h2>Past Events</h2>
		</div>
		<div class="photogallery-grid is-extra-wide">

		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					
			<article class="archive-result past-event">

			<?php $photo_gallery = get_field('event_photo_gallery'); ?>
			<?php if( $photo_gallery ): ?>
				<?php foreach( $photo_gallery as $photo_gallery ): ?>
					<?php $permalink = get_permalink( $photo_gallery->ID ); ?>
					<?php $title = get_the_title( $photo_gallery->ID ); ?>
					<?php $gallery = get_field('gallery', $photo_gallery->ID); ?>

					<a href="<?php echo esc_url( $permalink ); ?>">
						<div class="photo-stack">
							<?php $image1 = $gallery[0]; ?>
							<?php $image2 = $gallery[1]; ?>
							<?php $image3 = $gallery[2]; ?>

							<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image1['sizes']['placeholder']; ?>" data-src="<?php echo $image1['sizes']['small']; ?>" data-srcset="<?php echo $image1['sizes']['small']; ?> 350w, <?php echo $image1['sizes']['small']; ?> 700w, <?php echo $image1['sizes']['small']; ?> 1000w, <?php echo $image1['sizes']['small']; ?> 1200w"  alt="<?php echo $image1['alt']; ?>">
							<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image2['sizes']['placeholder']; ?>" data-src="<?php echo $image2['sizes']['small']; ?>" data-srcset="<?php echo $image2['sizes']['small']; ?> 350w, <?php echo $image2['sizes']['small']; ?> 700w, <?php echo $image2['sizes']['small']; ?> 1000w, <?php echo $image2['sizes']['small']; ?> 1200w"  alt="<?php echo $image2['alt']; ?>">
							<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image3['sizes']['placeholder']; ?>" data-src="<?php echo $image3['sizes']['small']; ?>" data-srcset="<?php echo $image3['sizes']['small']; ?> 350w, <?php echo $image3['sizes']['small']; ?> 700w, <?php echo $image3['sizes']['small']; ?> 1000w, <?php echo $image3['sizes']['small']; ?> 1200w"  alt="<?php echo $image3['alt']; ?>">
							<div class="button">View</div>
						</div>
						<header>
							<h3><?php echo esc_html( $title ); ?></h3>
						</header>
					</a>

				<?php endforeach; ?>
			<?php endif; ?>
			</article>

		<?php endwhile; ?>

		<?php wp_reset_postdata(); ?>

		</div>
	</section>
<?php endif; ?>

<?php get_template_part('template-parts/sections/testimonials'); ?>

<?php get_template_part('template-parts/sections/sponsors'); ?>

<?php get_template_part('template-parts/sections/donation-callout'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>