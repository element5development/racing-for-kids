<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head is-narrow">
	<h1><?php the_field($post_type.'_title','options'); ?></h1>

	<?php if ( get_field($post_type.'_intro','options') ) : ?>
	<p class="subtitle"><?php the_field($post_type.'_intro','options'); ?></p>
	<?php endif; ?>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>

			<section class="news-grid is-extra-wide">
				<?php	while ( have_posts() ) : the_post(); ?>

					<article class="archive-result <?php echo $post_type; ?>">
						<a href="<?php the_permalink(); ?>">
							<figure>
								<?php $postimage = get_field('post_image'); ?>
								<?php if( !empty( $postimage ) ): ?>
									<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $postimage['sizes']['placeholder']; ?>" data-src="<?php echo $postimage['sizes']['medium']; ?>" data-srcset="<?php echo $postimage['sizes']['small']; ?> 350w, <?php echo $postimage['sizes']['medium']; ?> 700w, <?php echo $postimage['sizes']['medium']; ?> 1000w, <?php echo $postimage['sizes']['medium']; ?> 1200w"  alt="<?php echo $postimage['alt']; ?>">
								<?php else : ?>
									<?php $defaultimage = get_field('default_news_image', 'options'); ?>
									<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $defaultimage['sizes']['placeholder']; ?>" data-src="<?php echo $defaultimage['sizes']['medium']; ?>" data-srcset="<?php echo $defaultimage['sizes']['small']; ?> 350w, <?php echo $defaultimage['sizes']['medium']; ?> 700w, <?php echo $defaultimage['sizes']['medium']; ?> 1000w, <?php echo $defaultimage['sizes']['medium']; ?> 1200w"  alt="<?php echo $defaultimage['alt']; ?>">
								<?php endif; ?>
							</figure>
							<header>
								<h3><?php the_title(); ?></h3>
							</header>
							<div class="entry-content">
								<p><?php the_excerpt(); ?></p>
								<div class="button">Read More</div>
							</div>
						</a>
					</article>

				<?php endwhile; ?>
			</section>

		<?php else : ?>
			<article>
				<section class="is-narrow">
					<h2>Coming soon.</h2>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/donation-callout'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>