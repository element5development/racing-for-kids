<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation">
	<nav>
		<a href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/racing-for-kids-logo.png" alt="Racing for Kids logo" />
		</a>
		<div>
			<button class="close">Close</button>
			<div class="social">
				<a href="<?php the_field('facebook', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#facebook" />
					</svg>
				</a>
				<a href="<?php the_field('twitter', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#twitter" />
					</svg>
				</a>
				<a href="<?php the_field('instagram', 'options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#instagram" />
					</svg>
				</a>
			</div>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
		</div>
		<button class="activate-menu">Menu</button>
	</nav>
</div>