<?php 
/*----------------------------------------------------------------*\

	FEATURED POPUP
	cookies used must be cleared via WPengine support

\*----------------------------------------------------------------*/
?>
<div class="featured-popup">
	<div>
		<?php $link = get_field('featured_event_link', 'options'); ?>
		<?php
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		?>
		<div class="date">
			<p><?php the_field('featured_event_month', 'options'); ?><span><?php the_field('featured_event_day', 'options'); ?></span></p>
		</div>
		<div class="info">
			<div>
				<h3><?php the_field('featured_event_title', 'options'); ?></h3>
				<p><?php the_field('featured_event_description', 'options'); ?></p>
			</div>
			<?php if ( $link ) : ?>
			<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
			<?php endif; ?>
		</div>
		<button>
			<svg><use xlink:href="#close"></use></svg>
		</button>
	</div>
</div>