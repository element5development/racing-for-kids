<?php 
/*----------------------------------------------------------------*\

	RELATED PHOTOGALLERY SECTION

\*----------------------------------------------------------------*/
?>

<?php $args = array(
		'post_type' => 'photogallery',
		'posts_per_page' => 3,
		'post__not_in' => array($post->ID),
	);
?>
<?php $my_query = new WP_Query( $args ); ?>

<?php if( $my_query->have_posts() ): ?>
	<section class="related-photogallery">
		<div class="is-narrow">
			<h2>More Photo Galleries</h2>
		</div>
		<div class="photogallery-grid is-extra-wide">

			<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

				<article class="archive-result photogallery">
					<?php $gallery = get_field('gallery', $photo_gallery->ID); ?>

					<a href="<?php the_permalink(); ?>">
						<div class="photo-stack">
							<?php $image1 = $gallery[0]; ?>
							<?php $image2 = $gallery[1]; ?>
							<?php $image3 = $gallery[2]; ?>

							<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image1['sizes']['placeholder']; ?>" data-src="<?php echo $image1['sizes']['small']; ?>" data-srcset="<?php echo $image1['sizes']['small']; ?> 350w, <?php echo $image1['sizes']['small']; ?> 700w, <?php echo $image1['sizes']['small']; ?> 1000w, <?php echo $image1['sizes']['small']; ?> 1200w"  alt="<?php echo $image1['alt']; ?>">
							<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image2['sizes']['placeholder']; ?>" data-src="<?php echo $image2['sizes']['small']; ?>" data-srcset="<?php echo $image2['sizes']['small']; ?> 350w, <?php echo $image2['sizes']['small']; ?> 700w, <?php echo $image2['sizes']['small']; ?> 1000w, <?php echo $image2['sizes']['small']; ?> 1200w"  alt="<?php echo $image2['alt']; ?>">
							<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image3['sizes']['placeholder']; ?>" data-src="<?php echo $image3['sizes']['small']; ?>" data-srcset="<?php echo $image3['sizes']['small']; ?> 350w, <?php echo $image3['sizes']['small']; ?> 700w, <?php echo $image3['sizes']['small']; ?> 1000w, <?php echo $image3['sizes']['small']; ?> 1200w"  alt="<?php echo $image3['alt']; ?>">
							<div class="button">View</div>
						</div>
						<header>
							<h3><?php the_title(); ?></h3>
						</header>
					</a>
				</article>

			<?php endwhile; ?>

			<?php wp_reset_postdata(); ?>
		</div>
	</section>
<?php endif; ?>