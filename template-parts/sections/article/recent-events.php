<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	recent events

\*----------------------------------------------------------------*/
?>

<?php $recent_events = get_sub_field('show_recent_events'); ?>
<?php if( $recent_events ): ?>
	<?php $args = array(
			'post_type' => 'event',
			'posts_per_page' => 3,
			'meta_query' => array(
				array(
						'key'   => 'upcoming_event',
						'value' => '0',
				)
			),
		);
	?>
	<?php $loop = new WP_Query( $args ); ?>

	<?php if( $loop->have_posts() ): ?>
	<section class="recent-events">
		<div class="is-narrow">
			<h2>Recent Events</h2>
		</div>
		<div class="photogallery-grid is-extra-wide">

		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					
			<article class="archive-result past-event">

			<?php $photo_gallery = get_field('event_photo_gallery'); ?>
			<?php if( $photo_gallery ): ?>
				<?php foreach( $photo_gallery as $photo_gallery ): ?>
					<?php $permalink = get_permalink( $photo_gallery->ID ); ?>
					<?php $title = get_the_title( $photo_gallery->ID ); ?>
					<?php $gallery = get_field('gallery', $photo_gallery->ID); ?>

					<a href="<?php echo esc_url( $permalink ); ?>">
						<div class="photo-stack">
							<?php $image1 = $gallery[0]; ?>
							<?php $image2 = $gallery[1]; ?>
							<?php $image3 = $gallery[2]; ?>

							<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image1['sizes']['placeholder']; ?>" data-src="<?php echo $image1['sizes']['small']; ?>" data-srcset="<?php echo $image1['sizes']['small']; ?> 350w, <?php echo $image1['sizes']['small']; ?> 700w, <?php echo $image1['sizes']['small']; ?> 1000w, <?php echo $image1['sizes']['small']; ?> 1200w"  alt="<?php echo $image1['alt']; ?>">
							<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image2['sizes']['placeholder']; ?>" data-src="<?php echo $image2['sizes']['small']; ?>" data-srcset="<?php echo $image2['sizes']['small']; ?> 350w, <?php echo $image2['sizes']['small']; ?> 700w, <?php echo $image2['sizes']['small']; ?> 1000w, <?php echo $image2['sizes']['small']; ?> 1200w"  alt="<?php echo $image2['alt']; ?>">
							<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image3['sizes']['placeholder']; ?>" data-src="<?php echo $image3['sizes']['small']; ?>" data-srcset="<?php echo $image3['sizes']['small']; ?> 350w, <?php echo $image3['sizes']['small']; ?> 700w, <?php echo $image3['sizes']['small']; ?> 1000w, <?php echo $image3['sizes']['small']; ?> 1200w"  alt="<?php echo $image3['alt']; ?>">
							<div class="button">View</div>
						</div>
						<header>
							<h3><?php echo esc_html( $title ); ?></h3>
						</header>
					</a>

				<?php endforeach; ?>
			<?php endif; ?>
			</article>

		<?php endwhile; ?>

		<?php wp_reset_postdata(); ?>

		</div>
	</section>
	<?php endif; ?>
<?php endif; ?>