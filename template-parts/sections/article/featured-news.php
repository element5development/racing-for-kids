<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	featured news

\*----------------------------------------------------------------*/
?>

<?php $featured_posts = get_sub_field('featured_posts'); ?>
<?php if( $featured_posts ): ?>
<section class="featured-news">
	<div class="is-narrow">
		<h2>Featured News</h2>
	</div>
	<div class="news-grid standard">

	<?php foreach( $featured_posts as $featured_post ): ?>
		<?php $permalink = get_permalink( $featured_post->ID ); ?>
		<?php $title = get_the_title( $featured_post->ID ); ?>
		<?php $excerpt = get_the_excerpt( $featured_post->ID ); ?>
		<?php $postimage = get_field('post_image', $featured_post->ID); ?>

		<article class="archive-result post">
			<a href="<?php echo esc_url( $permalink ); ?>">
				<figure>
					<?php if( !empty( $postimage ) ): ?>
						<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $postimage['sizes']['placeholder']; ?>" data-src="<?php echo $postimage['sizes']['medium']; ?>" data-srcset="<?php echo $postimage['sizes']['small']; ?> 350w, <?php echo $postimage['sizes']['medium']; ?> 700w, <?php echo $postimage['sizes']['medium']; ?> 1000w, <?php echo $postimage['sizes']['medium']; ?> 1200w"  alt="<?php echo $postimage['alt']; ?>">
					<?php else : ?>
						<?php $defaultimage = get_field('default_news_image', 'options'); ?>
						<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $defaultimage['sizes']['placeholder']; ?>" data-src="<?php echo $defaultimage['sizes']['medium']; ?>" data-srcset="<?php echo $defaultimage['sizes']['small']; ?> 350w, <?php echo $defaultimage['sizes']['medium']; ?> 700w, <?php echo $defaultimage['sizes']['medium']; ?> 1000w, <?php echo $defaultimage['sizes']['medium']; ?> 1200w"  alt="<?php echo $defaultimage['alt']; ?>">
					<?php endif; ?>
				</figure>
				<header>
					<h3><?php echo esc_html( $title ); ?></h3>
				</header>
				<div class="entry-content">
					<p><?php echo esc_html( $excerpt ); ?></p>
					<div class="button">Read More</div>
				</div>
			</a>
		</article>

	<?php endforeach; ?>

	</div>

</section>
<?php endif; ?>