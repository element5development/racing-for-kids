<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	testimonials

\*----------------------------------------------------------------*/
?>

<?php $show_testimonials = get_sub_field('show_testimonials'); ?>
<?php if( $show_testimonials ): ?>
	
<?php get_template_part('template-parts/sections/testimonials'); ?>

<?php endif; ?>