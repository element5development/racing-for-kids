<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	sponsors

\*----------------------------------------------------------------*/
?>

<?php $show_sponsors = get_sub_field('show_sponsors'); ?>
<?php if( $show_sponsors ): ?>

<?php get_template_part('template-parts/sections/sponsors'); ?>

<?php endif; ?>