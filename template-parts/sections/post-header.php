<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head">
	<div class="is-narrow">
		<?php $post_date = get_the_date( 'F j, Y' ); ?>
		<p class="date"><strong><?php echo $post_date; ?></strong></p>
		<h1><?php the_title(); ?></h1>
		<?php $post_author = get_the_author_meta( 'display_name', $post->post_author ); ?>
		<p>by <?php echo $post_author; ?></p>
	</div>
	<?php $postimage = get_field('post_image'); ?>
	<?php if( !empty( $postimage ) ): ?>
	<div class="standard">
		<img src="<?php echo esc_url($postimage['url']); ?>" alt="<?php echo esc_attr($postimage['alt']); ?>" />
	</div>
	<?php endif; ?>
</header>