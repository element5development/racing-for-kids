<?php 
/*----------------------------------------------------------------*\

	HOMEPAGE HEADER

\*----------------------------------------------------------------*/
?>

<header class="post-head homepage-head is-extra-wide">
	<div>
		<?php if ( get_field('page_title') ) : ?>
		<h1><?php the_field('page_title'); ?></h1>
		<?php else : ?>
		<h1><?php the_title(); ?></h1>
		<?php endif; ?>

		<?php if ( get_field('page_description') ) : ?>
		<p class="subtitle"><?php the_field('page_description'); ?></p>
		<?php endif; ?>

		<?php $button = get_field('button'); ?>
		<?php if( $button ): ?>
		<a class="button" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
		<?php endif; ?>
	</div>
	<div class="image-wrap">
		<?php $image = get_field('featured_image'); ?>
		<?php if( $image ): ?>
		<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
		<?php endif; ?>
	</div>
</header>