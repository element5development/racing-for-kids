<?php 
/*----------------------------------------------------------------*\

	PAGE HEADER

\*----------------------------------------------------------------*/
?>

<header class="post-head page-head is-narrow">
	<?php if ( get_field('page_title') ) : ?>
	<h1><?php the_field('page_title'); ?></h1>
	<?php else : ?>
	<h1><?php the_title(); ?></h1>
	<?php endif; ?>

	<?php if ( get_field('page_description') ) : ?>
	<p class="subtitle"><?php the_field('page_description'); ?></p>
	<?php endif; ?>
</header>