<?php 
/*----------------------------------------------------------------*\

	DONATION SECTION

\*----------------------------------------------------------------*/
?>
<?php $donation = get_field('make_a_donation','options'); ?>
<?php if( $donation ): ?>
<section class="donation-callout standard aligncenter">
	<div>
		<h3>Honoring Racing for Kids Courageous Kids</h3>
		<img src="<?php echo esc_url( $donation['image']['url'] ); ?>" alt="<?php echo esc_attr( $donation['image']['alt'] ); ?>" />
		<h2><?php echo $donation['title']; ?></h2>
		<a class="button" href="<?php echo esc_url( $donation['button']['url'] ); ?>" target="_blank"><?php echo esc_html( $donation['button']['title'] ); ?></a>
	</div>
</section>
<?php endif; ?>