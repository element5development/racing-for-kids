<?php 
/*----------------------------------------------------------------*\

	SPONSORS SECTION

\*----------------------------------------------------------------*/
?>

<?php if( have_rows('sponsors', 'options') ): ?>
<section class="sponsors">
	<div class="is-narrow">
		<h2>Our Sponsors</h2>
	</div>
	<div class="gallery standard columns-5">
		<?php while( have_rows('sponsors', 'options') ) : the_row(); ?>

		<?php $sponsor = get_sub_field('sponsor', 'options'); ?>
		<figure>
			<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $sponsor['sizes']['placeholder']; ?>" data-src="<?php echo $sponsor['sizes']['large']; ?>" data-srcset="<?php echo $sponsor['sizes']['small']; ?> 350w, <?php echo $sponsor['sizes']['medium']; ?> 700w, <?php echo $sponsor['sizes']['large']; ?> 1000w, <?php echo $sponsor['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $sponsor['alt']; ?>">
		</figure>

		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>