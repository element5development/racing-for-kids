<?php 
/*----------------------------------------------------------------*\

	RELATED NEWS SECTION

\*----------------------------------------------------------------*/
?>
<?php $args = array(
		'post_type' => 'post',
		'posts_per_page' => 3,
		'post__not_in' => array($post->ID),
	);
?>
<?php $my_query = new WP_Query( $args ); ?>

<?php if( $my_query->have_posts() ): ?>
	<section class="related-news">
		<div class="is-narrow">
			<h2>More News</h2>
		</div>
		<div class="news-grid is-extra-wide">

			<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

				<article class="archive-result post">
					<a href="<?php the_permalink(); ?>">
						<figure>
							<?php $postimage = get_field('post_image'); ?>
							<?php if( !empty( $postimage ) ): ?>
								<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $postimage['sizes']['placeholder']; ?>" data-src="<?php echo $postimage['sizes']['medium']; ?>" data-srcset="<?php echo $postimage['sizes']['small']; ?> 350w, <?php echo $postimage['sizes']['medium']; ?> 700w, <?php echo $postimage['sizes']['medium']; ?> 1000w, <?php echo $postimage['sizes']['medium']; ?> 1200w"  alt="<?php echo $postimage['alt']; ?>">
							<?php else : ?>
								<?php $defaultimage = get_field('default_news_image', 'options'); ?>
								<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $defaultimage['sizes']['placeholder']; ?>" data-src="<?php echo $defaultimage['sizes']['medium']; ?>" data-srcset="<?php echo $defaultimage['sizes']['small']; ?> 350w, <?php echo $defaultimage['sizes']['medium']; ?> 700w, <?php echo $defaultimage['sizes']['medium']; ?> 1000w, <?php echo $defaultimage['sizes']['medium']; ?> 1200w"  alt="<?php echo $defaultimage['alt']; ?>">
							<?php endif; ?>
						</figure>
						<header>
							<h3><?php the_title(); ?></h3>
						</header>
						<div class="entry-content">
							<p><?php the_excerpt(); ?></p>
							<div class="button">Read More</div>
						</div>
					</a>
				</article>

			<?php endwhile; ?>

			<?php wp_reset_postdata(); ?>
		</div>
	</section>
<?php endif; ?>