<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div>
		<?php wp_nav_menu(array( 'theme_location' => 'footer_navigation' )); ?>
		<div class="social">
			<a href="<?php the_field('facebook', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#facebook" />
				</svg>
			</a>
			<a href="<?php the_field('twitter', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#twitter" />
				</svg>
			</a>
			<a href="<?php the_field('instagram', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#instagram" />
				</svg>
			</a>
		</div>
		<div id="element5-credit">
			<a target="_blank" href="https://element5digital.com" rel="nofollow">
				<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
			</a>
		</div>
		<div class="copyright">
			<p>© Copyright <?php echo date('Y'); ?>. <?php echo get_bloginfo( 'name' ); ?>® is an official 501 (c) (3) not-for-profit foundation. All Rights Reserved. This site is compliant with the CCPA and GDPR.</p>
		</div>
	</div>
</footer>