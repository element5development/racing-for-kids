<?php 
/*----------------------------------------------------------------*\

	TESTIMONIALS SECTION

\*----------------------------------------------------------------*/
?>
<?php if( have_rows('testimonials', 'options') ): ?>
<section class="testimonials">
	<hr>
	<div class="testimonial-slider is-narrow">
		<?php while( have_rows('testimonials', 'options') ): the_row(); ?>
			<?php $headshot = get_sub_field('headshot', 'options'); ?>

			<blockquote>
				<p><?php the_sub_field('testimonial', 'options'); ?></p>
				<cite>
					<img src="<?php echo $headshot['sizes']['small']; ?>" alt="<?php echo $headshot['alt']; ?>">
					<div>
						<h5><?php the_sub_field('name', 'options'); ?></h5>
						<p><?php the_sub_field('info', 'options'); ?></p>
					</div>
				</cite>
			</blockquote>

		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>