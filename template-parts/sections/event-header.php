<?php 
/*----------------------------------------------------------------*\

	EVENT HEADER

\*----------------------------------------------------------------*/
?>

<header class="post-head event-head is-narrow">
	<?php $upcoming = get_field('upcoming_event'); ?>
	<?php if( $upcoming ): ?>
		<p>Upcoming Event</p>
	<?php endif; ?>
	<h1><?php the_title(); ?></h1>

	<div class="event-info">
		<h3><?php the_field('event_date'); ?></h3>
		<p><?php the_field('event_location'); ?></p>
	</div>
</header>